package com.shymoniak.model;

import com.shymoniak.model.task1.IntervalObj;
import com.shymoniak.model.task1.Task1;

import java.util.List;

public class MainModel {
    Task1 task1 = new Task1();

    public void printTask1() {
        int[] randArr = new int[]{0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 4, 4, 4};
        List<IntervalObj> resList = task1.getAllSequences(randArr);
        resList.stream().forEach(s -> System.out.println(s));
    }
}