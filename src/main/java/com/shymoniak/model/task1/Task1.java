package com.shymoniak.model.task1;

import java.util.ArrayList;
import java.util.List;

/**
 * Given an array of integers, find the length and location of the longest contiguous
 * sequence of equal values where the values of the elements just before and just after
 * this sequence are smaller.
 */
public class Task1 {

    private List<IntervalObj> intervalObjList = new ArrayList<>();

    public List<IntervalObj> getAllSequences(int[] arr) {
        int[] sequenceIndex = new int[2];
        sequenceIndex[0] = 0;
        sequenceIndex[1] = 0;

        do {
            if (sequenceIndex[1] < arr.length) {
                sequenceIndex = findSequenceIndexes(arr, sequenceIndex[1]);
                if (sequenceIndex[0] >= 0 && sequenceIndex[1] >= 0) {
                    IntervalObj intervalObj = new IntervalObj();
                    intervalObj.setStartIndex(sequenceIndex[0]);
                    intervalObj.setEndIndex(sequenceIndex[1]);
                    intervalObj.setSymbol(arr[sequenceIndex[0]]);
                    intervalObjList.add(intervalObj);
                } else {
                    break;
                }
            }
        } while (sequenceIndex[0] != sequenceIndex[1]);
        if (intervalObjList.size() == 0){
            intervalObjList.add(new IntervalObj());
        }
        return intervalObjList;
    }

    public int[] findSequenceIndexes(int[] arr, int beginWith) {
        int[] res = new int[2];
        int firstIndex = -1;
        int lastIndex = -1;

        int prev = arr[beginWith];
        int curr;
        for (int i = beginWith + 1; i < arr.length; i++) {
            curr = arr[i];
            if (curr == prev) {
                if (firstIndex == -1) {
                    firstIndex = i - 1;
                }
                while (curr == prev && i < arr.length) {
                    lastIndex = i;
                    prev = curr;
                    if (i + 1 < arr.length) {
                        curr = arr[++i];
                    } else {
                        break;
                    }
                }
                break;
            } else {
                prev = curr;
            }
        }
        res[0] = firstIndex;
        res[1] = lastIndex;
        return res;
    }
}