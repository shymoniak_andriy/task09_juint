package com.shymoniak.model.task1;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class IntervalObj {

    private int startIndex;
    private int endIndex;
    private int symbol;

    public int getLength() {
        return endIndex - startIndex + 1;
    }

    @Override
    public String toString() {
        return "[" + startIndex +
                ", " + endIndex +
                "] - length = " + getLength() +
                " (symbol = " + symbol + ")";
    }
}
