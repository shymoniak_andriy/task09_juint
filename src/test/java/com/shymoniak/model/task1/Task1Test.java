package com.shymoniak.model.task1;

import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.TestRule;
import org.junit.rules.Timeout;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class Task1Test {

    @Rule
    public final TestRule timeout = Timeout.seconds(1);

    Task1 task1 = new Task1();
//    Task1 task1 = Mockito.mock(Task1.class);
        
    @Test()
    public void testCasualCase() {
        int[] randArr = new int[]{4, 4, 4};
        List<IntervalObj> resList = task1.getAllSequences(randArr);
        assertEquals(new IntervalObj(0, 2, 4), resList.get(0));
    }

    @Test
    public void testBigData() {
        int[] randArr = new int[]{0, 0, 3, 4, 5, 1, 1, 1, 1, 1, 2, 2, 2, 2, 4, 4, 4, 88, 9, 0, 2, 8,
                555, 555, 555, 555, 555, 555, 555, 5, 55, 555, 16, 1, 16, 24, 88};
        List<IntervalObj> resList = task1.getAllSequences(randArr);
        assertEquals(new IntervalObj(22, 28, 555), resList.get(4));
    }

    @Test
    public void testNegativeValues() {
        int[] randArr = new int[]{-444, -444, -444};
        List<IntervalObj> resList = task1.getAllSequences(randArr);
        assertEquals(new IntervalObj(0, 2, -444), resList.get(0));
    }

    @Test
    public void testNullValues() {
        int[] randArr = new int[]{4, 1, 2};
        List<IntervalObj> resList = task1.getAllSequences(randArr);
        assertEquals(new IntervalObj(), resList.get(0));
    }
}